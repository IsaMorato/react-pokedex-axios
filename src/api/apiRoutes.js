const BASE_URL =" https://pokeapi.co/api/v2/"
const GET_POKEMONS= `${BASE_URL}/pokemon`


export{
    BASE_URL,
    GET_POKEMONS
}